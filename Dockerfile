FROM alpine:3.9

LABEL Name=Terragrunt \
  Version=edge

ENV TERRAGRUNT_VERSION=0.19.24
ENV TERRAGRUNT_TFPATH=/bin/terraform
# Configure Go
ENV GOROOT /usr/lib/go
ENV GOPATH /go
ENV PATH /go/bin:$PATH

RUN mkdir -p ${GOPATH}/src ${GOPATH}/bin
ENV PATH="/usr/local/go/bin:$PATH"
ENV GOPATH=/opt/go/
ENV PATH=$PATH:$GOPATH/bin

EXPOSE 0

# Install utils
RUN apk update && apk add --no-cache bash ca-certificates git make musl-dev go unzip curl openssh python py-pip && \
  pip install --upgrade awscli s3cmd && \
  apk -v --purge del py-pip && \
  rm /var/cache/apk/*

# Install terraform
RUN curl https://releases.hashicorp.com/terraform/0.12.17/terraform_0.12.17_linux_amd64.zip -o terraform.zip \
  && unzip terraform.zip \
  && mv terraform /bin \
  && rm terraform.zip

# Install terragrunt
RUN curl -sL https://github.com/gruntwork-io/terragrunt/releases/download/v$TERRAGRUNT_VERSION/terragrunt_linux_amd64 -o /bin/terragrunt && chmod +x /bin/terragrunt

# Install kubectl
RUN curl -L https://storage.googleapis.com/kubernetes-release/release/v1.11.3/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl && \
  chmod +x /usr/local/bin/kubectl 

# Install helm 
RUN curl -L https://storage.googleapis.com/kubernetes-helm/helm-v2.12.3-linux-amd64.tar.gz | tar xz && mv linux-amd64/helm /bin/helm && rm -rf linux-amd64 && \
  chmod +x /bin/helm 

# Install aws-iam-authenticator

# Install go dep
RUN mkdir -p /opt/go/bin/ && curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh

# SSH Config
RUN mkdir ~/.ssh
RUN echo $'Host * \n\
  StrictHostKeyChecking no \n\
  UserKnownHostsFile=/dev/null\n' \
  >> ~/.ssh/config

# Add SA script, we can use this to authenticate against kube and gerate a kubeconfig
ADD get-sa.sh /get-sa.sh

CMD ["bash"]
